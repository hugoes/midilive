
import Dexie from 'dexie';
import song from './models/song.model';
var db = new Dexie('live');

db.version(1).stores({
  genre: `++id, name`,
  songs: `++id, name, sections, genre_id, status`,
});

db.songs.mapToClass(song(db));
// db.songs.clear();
db.songs.toArray().then(items =>{
  if(items.length < 1) {
    db.songs.add({
      name : "Song 1",
      "sections": [
        {
          "key_zone": {
            "first": {"index": 24, "value":"C1"},
            "last":{"index": 48, "value":"C3"}
          },
          "midi_channel": {"index": 0, "value": 1},
          "level": {"index": 3, "value": 0},
          "pan": {"index": 64, "value": "C"},
          "transpose": {"index": 3, "value": 12},
          "output": {"index": 0, "value": 0}
        },
        {
          "key_zone": {
            "first": {"index": 24, "value":"C1"},
            "last":{"index": 48, "value":"C3"}
          },
          "midi_channel": {"index": 0, "value": 1},
          "level": {"index": 3, "value": 0},
          "pan": {"index": 64, "value": "C"},
          "transpose": {"index": 3, "value": 12},
          "output": {"index": 1, "value": 1},
          "harmonizer": {
            "chords": [
              {
                "midi_channel": {"index": 0, "value": 1},
                "level": {"index": 3, "value": 0},
                "pan": {"index": 64, "value": "C"},
                "transpose": {"index": 3, "value": 12},
                "notes": [
                  {"index": 64, "value": 68},
                  {"index": 66, "value": 69}
                ]
              }
            ]
          }
        }
      ]
  });
  db.songs.add({
    name : "Song 2",
    "sections": [
      {
        "key_zone": {
          "first": {"index": 24, "value":"C1"},
          "last":{"index": 49, "value":"C4"}
        },
        "midi_channel": {"index": 0, "value": 1},
        "level": {"index": 3, "value": 0},
        "pan": {"index": 64, "value": "C"},
        "transpose": {"index": 3, "value": 12},
        "output": {"index": 0, "value": 0}
      },
      {
        "key_zone": {
          "first": {"index": 25, "value":"C2"},
          "last":{"index": 48, "value":"C3"}
        },
        "midi_channel": {"index": 0, "value": 1},
        "level": {"index": 3, "value": 0},
        "pan": {"index": 64, "value": "C"},
        "transpose": {"index": 3, "value": 12},
        "output": {"index": 2, "value": 3},
        "harmonizer": {
          "chords": [
            {
              "midi_channel": {"index": 0, "value": 1},
              "level": {"index": 3, "value": 0},
              "pan": {"index": 64, "value": "C"},
              "transpose": {"index": 3, "value": 12},
              "notes": [
                {"index": 64, "value": 68},
                {"index": 66, "value": 69}
              ]
            },
            {
              "midi_channel": {"index": 0, "value": 1},
              "level": {"index": 3, "value": 0},
              "pan": {"index": 64, "value": "C"},
              "transpose": {"index": 3, "value": 12},
              "notes": [
                {"index": 64, "value": 68},
                {"index": 66, "value": 69}
              ]
            }
          ]
        }
      }
    ]
  });
  }
});
export default db;