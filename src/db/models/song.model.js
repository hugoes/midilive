
export default function(db) {
    return class Song {
        constructor(name, sections, genre_id, status) {
            this.name = name;
            this.sections = sections;
            this.genre_id = genre_id;
            this.status = status;
        }
        // --------------------------------------------------
        save() {
            db.songs.put(this);
        }
        // --------------------------------------------------
        delete() {
            db.songs.delete(this.id);
        }
        // --------------------------------------------------
    }
}
