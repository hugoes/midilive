import { midi } from '../constants';
export default class SongActions {
    //--------------------------------------------------------------------------------------------
    static setInputs(payload) {
        return {
            type: midi.SET_INPUTS,
            payload 

        }
    }
    //--------------------------------------------------------------------------------------------
    static setOutputs(payload) {
        return {
            type: midi.SET_OUTPUTS,
            payload 

        }
    }
    //--------------------------------------------------------------------------------------------
    static setActiveInput(payload) {
        return {
            type: midi.SET_ACTIVE_INPUT,
            payload 

        }
    }
    //--------------------------------------------------------------------------------------------
    static addActiveOutput(payload) {
        return {
            type: midi.ADD_ACTIVE_OUTPUT,
            payload 

        }
    }
    //--------------------------------------------------------------------------------------------
    static removeActiveOutput(payload) {
        return {
            type: midi.REMOVE_ACTIVE_OUTPUT,
            payload 

        }
    }
    //--------------------------------------------------------------------------------------------
}