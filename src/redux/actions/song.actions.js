import { songs } from '../constants';
import db from '../../db/db';
export default class SongActions {
    //--------------------------------------------------------------------------------------------
    static loadAll() {
        return (dispatch) => { 
            db.songs.toArray()
                .then(payload => {
                    dispatch({
                        type: songs.LOAD_ALL,
                        payload 
                    });
                });
        }
        
    }
    //--------------------------------------------------------------------------------------------
    static add(payload) {
        return {
            type: songs.ADD,
            payload 
        }
    }
    //--------------------------------------------------------------------------------------------
    static setCurrent(payload) {
        return {
            type: songs.SET_CURRENT,
            payload, 
        }
    }
}