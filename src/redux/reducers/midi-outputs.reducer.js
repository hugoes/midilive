import { midi } from '../constants';

export default function(state = [], action) {
    switch(action.type) {
        case midi.SET_OUTPUTS:
            return action.payload;
        default:
            return state;
    }
}
