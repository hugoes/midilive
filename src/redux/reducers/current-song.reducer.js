import { songs } from '../constants';

export default function(state = null, action) {
    switch(action.type) {
        case songs.SET_CURRENT:
            return action.payload;
        default:
            return state;
    }
}
