import { combineReducers } from 'redux'
import songs from './songs.reducer';
import currentSong from './current-song.reducer';
import midiInputs from './midi-inputs.reducer';
import midiOutputs from './midi-outputs.reducer';
import activeMidiInput from './active-midi-input.reducer';
import selectedMidiOutputs from './selected-midi-outputs.reducer';

export default combineReducers({
    songs,
    currentSong,
    midiInputs,
    midiOutputs,
    activeMidiInput,
    selectedMidiOutputs
});