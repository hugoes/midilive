import { midi } from '../constants';

export default function(state = [], action) {
    switch(action.type) {
        case midi.ADD_ACTIVE_OUTPUT:
            return [...state, action.payload];
        case midi.REMOVE_ACTIVE_OUTPUT:
            return state.filter(output => output.id !== action.payload.id);
        default:
            return state;
    }
}
