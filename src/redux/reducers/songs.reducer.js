import { songs } from '../constants';

export default function(state = [], action) {
    switch(action.type) {
        case songs.LOAD_ALL:
            return action.payload;
        case songs.ADD:
            return [...state, action.payload];
        default:
            return state;
    }
}