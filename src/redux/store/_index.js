import { createStore ,compose, applyMiddleware } from 'redux'
import midiApp from '../reducers/_index';
import thunk from 'redux-thunk';

const initialState = {};

export default createStore(
    midiApp,
    initialState,
    compose(
        applyMiddleware(thunk)
    )
);