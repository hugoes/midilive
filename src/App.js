import React, { Component } from 'react';
import './App.scss';
import Tabs from './components/tabs/tabs.component';
import Tab from './components/tabs/tab-content/tab-content.component';
import Songs from './containers/songs.container';
import Song from './containers/song.container';
import Midi from './containers/midi.container';

class App extends Component {
  //--------------------------------------------------------------------------
  render() {
    return (
      <div className="App">
        <Tabs>
          {/* Tab 2*/}
          <Tab label="Live">
            <div className="song-container">
              {/*Song Select*/}
              <Songs midiApi={this.props.midiApi}/>
              <Song/>
            </div>
          </Tab>
          {/* Tab 1*/}
          <Tab label="Midi">
            <Midi midiApi={this.props.midiApi}/>
          </Tab>
        </Tabs>
      </div>
    );
  }
}
export default App;
