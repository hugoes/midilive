import { connect } from 'react-redux';
import SongActions from '../redux/actions/song.actions';
import Songs from '../components/songs/songs.component';

const mapStateToProps = state => ({
  songs: state.songs,
  currentSong: state.currentSong,
});

const mapDispatchToProps = dispatch => ({
  loadAllSongs: () => dispatch(SongActions.loadAll()),
  addSong: song => dispatch(SongActions.add(song)),
  setCurrentSong: song => dispatch(SongActions.setCurrent(song))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Songs)