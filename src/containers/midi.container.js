import { connect } from 'react-redux';
import MidiActions from '../redux/actions/midi.actions';
import Midi from '../components/midi/midi.component';

const mapStateToProps = state => ({
  midiInputs: state.midiInputs,
  midiOutputs: state.midiOutputs,
  activeMidiInput: state.activeMidiInput,
  selectedMidiOutputs: state.selectedMidiOutputs
});

const mapDispatchToProps = dispatch => ({
  setInputs: inputs => dispatch(MidiActions.setInputs(inputs)),
  setOutputs: outputs => dispatch(MidiActions.setOutputs(outputs)),
  setActiveInput: input => dispatch(MidiActions.setActiveInput(input)),
  addActiveOutput: output => dispatch(MidiActions.addActiveOutput(output)),
  removeActiveOutput: output => dispatch(MidiActions.removeActiveOutput(output))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Midi)