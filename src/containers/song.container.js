import { connect } from 'react-redux';
import SongActions from '../redux/actions/song.actions';
import Song from '../components/song/song.component';

const mapStateToProps = state => ({
  currentSong: state.currentSong,
});

const mapDispatchToProps = dispatch => ({
  setCurrentSong: song => dispatch(SongActions.setCurrent(song))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Song)