import Notes from './notes';

export default {
    key_zone: {
        first: Notes,
        last: Notes,
    },
    midi_channel: [
        {index: 0, value: 1},
        {index: 1, value: 2},
        {index: 2, value: 3},
        {index: 3, value: 4},
        {index: 4, value: 5},
        {index: 5, value: 6},
        {index: 6, value: 7},
        {index: 7, value: 8},
        {index: 8, value: 9},
        {index: 9, value: 10},
        {index: 'A', value: 11},
        {index: 'B', value: 12},
        {index: 'C', value: 13},
        {index: 'D', value: 14},
        {index: 'E', value: 15},
        {index: 'F', value: 16}
    ],
    level: [
        {index: 0, value: -6},
        {index: 1, value: -4},
        {index: 2, value: -2},
        {index: 3, value: 0},
        {index: 4, value: 2},
        {index: 5, value: 4},
        {index: 6, value: 6},
    ],
    pan: [
        {index: 49, value: 'L50'},
        {index: 64, value: 'C'},
        {index: 79, value: 'R50'},
    ],
    transpose: [
        {index: 0, value: -24},
        {index: 1, value: -12},
        {index: 2, value: 0},
        {index: 3, value: 12},
        {index: 4, value: 24},
    ]
}