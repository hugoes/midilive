export default {
  "key_zone": {
    "first": {"index": 24, "value":"C1"},
    "last":{"index": 48, "value":"C3"}
  },
  "midi_channel": {"index": 0, "value": 1},
  "level": {"index": 3, "value": 0},
  "pan": {"index": 64, "value": "C"},
  "transpose": {"index": 3, "value": 12},
  "harmonizer": {
    "chords": [
    ]
  }
}