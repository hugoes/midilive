export default {
  "midi_channel": {"index": 0, "value": 1},
  "level": {"index": 3, "value": 0},
  "pan": {"index": 64, "value": "C"},
  "transpose": {"index": 3, "value": 12},
  "notes": [
    {"index": 64, "value": 68},
    {"index": 66, "value": 69}
  ]
}