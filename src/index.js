import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'

import './index.scss';
import App from './App';
import store from './redux/store/_index';

import * as serviceWorker from './serviceWorker';
import MidiApi from './vendor/midi-api/midi-api';

const midiApi =  new MidiApi();

ReactDOM.render(
    <Provider store={store} >
        <App midiApi={midiApi}/>
    </Provider>, 
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
