import React, { Component } from 'react';
import Instrument from '../instrument/instrument.component';
import PropTypes from 'prop-types';

export default class Song extends Component {
    static propTypes = {
        currentSong : PropTypes.object,
        setCurrentSong: PropTypes.func.isRequired,
    }
    //--------------------------------------------------------------------------
    constructor(props) {
        super(props);
        //console.log(props);
        this.onChangeValue = this.onChangeValue.bind(this);
    }
    //--------------------------------------------------------------------------------------------
    onChangeValue(value, contentIndex, sectionIndex) {
        const currentSong = {...this.props.currentSong};
        if(sectionIndex !== null) currentSong.sections[sectionIndex].harmonizer.chords[contentIndex] = value;
        else currentSong.sections[contentIndex] = value;
        this.props.setCurrentSong(currentSong);
    }
    //--------------------------------------------------------------------------
    render() {
        let song = null;
        if(this.props.currentSong) {
            song = 
                <React.Fragment>
                    <div className="header">
                        <div>Key Zone</div>
                        <div>Midi Ch</div>
                        <div>Level</div>
                        <div>Pan</div>
                        <div>Transpose</div>
                    </div>
                    <div className="section">
                        <Instrument name="Section 1"
                            contentIndex={0} 
                            content={this.props.currentSong.sections[0]}
                            onChangeValue={this.onChangeValue}/>
                    </div>
                    <div className="section">
                        <Instrument name="Section 2"
                            contentIndex={1}  
                            content={this.props.currentSong.sections[1]}
                            onChangeValue={this.onChangeValue}/>
                        {/*Harmonizer*/}
                        {this.props.currentSong.sections[1] &&
                        <div className="harmonizer">
                            <Instrument name="Chord 1"
                            type={'chord'}
                            sectionIndex={1}
                            contentIndex={0} 
                            content={this.props.currentSong.sections[1].harmonizer.chords[0]}
                            onChangeValue={this.onChangeValue}/>
                            <Instrument name="Chord 2"
                            type={'chord'}
                            sectionIndex={1}
                            contentIndex={1} 
                            content={this.props.currentSong.sections[1].harmonizer.chords[1]}
                            onChangeValue={this.onChangeValue}/>
                        </div>}
                    </div>
                    {/*Section*/}
                    <div className="section">
                        <Instrument 
                            name="Section 3"
                            contentIndex={2}  
                            content={this.props.currentSong.sections[2]}
                            onChangeValue={this.onChangeValue}/>
                        {/*Harmonizer*/}
                        {this.props.currentSong.sections[2] &&
                        <div className="harmonizer">
                            <Instrument 
                                name="Chord 1"
                                type={'chord'}
                                sectionIndex={2}
                                contentIndex={0} 
                                content={this.props.currentSong.sections[2].harmonizer.chords[0]}
                                onChangeValue={this.onChangeValue}/>
                            <Instrument 
                                name="Chord 2"
                                type={'chord'}
                                sectionIndex={2}
                                contentIndex={1} 
                                content={this.props.currentSong.sections[2].harmonizer.chords[1]}
                                onChangeValue={this.onChangeValue}/>
                        </div>}
                    </div>
                    {/*Section*/}
                    <div className="section">
                        <Instrument 
                            name="Section 4"
                            contentIndex={3}  
                            content={this.props.currentSong.sections[3]}
                            onChangeValue={this.onChangeValue}/>
                        {/*Harmonizer*/}
                        {this.props.currentSong.sections[3] &&
                        <div className="harmonizer">
                            <Instrument name="Chord 1"
                                type={'chord'}
                                sectionIndex={3} 
                                contentIndex={0}
                                content={this.props.currentSong.sections[3].harmonizer.chords[0]}
                                onChangeValue={this.onChangeValue}/>
                            <Instrument name="Chord 2"
                                type={'chord'}
                                sectionIndex={3} 
                                contentIndex={1}
                                content={this.props.currentSong.sections[3].harmonizer.chords[1]}
                                onChangeValue={this.onChangeValue}/>
                        </div>}
                    </div>
                    {/*Section*/}
                    <div className="section">
                        <Instrument name="Section 5"
                            contentIndex={4}  
                            content={this.props.currentSong.sections[4]}
                            onChangeValue={this.onChangeValue}/>
                    </div>
                </React.Fragment>
        }
        return (
            song
        )
    }
    
}