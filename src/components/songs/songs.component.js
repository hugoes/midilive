import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Songs extends Component {
    static propTypes = {
        songs: PropTypes.array.isRequired,
        currentSong: PropTypes.object,
        loadAllSongs : PropTypes.func.isRequired,
        setCurrentSong: PropTypes.func.isRequired,
        addSong: PropTypes.func.isRequired,
        midiApi: PropTypes.object.isRequired,
    }
    //--------------------------------------------------------------------------
    constructor(props) {
        super(props);
        this.handleSongChange = this.handleSongChange.bind(this);
    }
    //--------------------------------------------------------------------------
    componentDidMount() {
        this.props.loadAllSongs();
    }
    //--------------------------------------------------------------------------
    componentWillReceiveProps(nextProps) {
        //console.log(nextProps);
        if(!nextProps.currentSong) {
            if(!nextProps.songs.length > 0) return; 
            this.props.setCurrentSong(nextProps.songs[0]);
        } else {
            this.props.midiApi.setCurrentSong(nextProps.currentSong);
        }
    }
    //--------------------------------------------------------------------------
    handleSongChange(event) {
        this.props.setCurrentSong(this.props.songs[event.target.value]);
    }
    //--------------------------------------------------------------------------
    render() {
        return (
            <div className="song-select">
                {this.props.songs.length > 0 ? (
                    <select onChange={this.handleSongChange}>
                        {this.props.songs.map((song, index) => 
                        <option key={index} value={index}>
                            {song.name}
                        </option>
                        )}
                    </select>
                ) : (
                    <h1>No hay songs par mostrar</h1>
                )}
            </div>
        )
    }
}