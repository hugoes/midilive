import React, { Component } from 'react';
import './tabs.component.scss';
import TabHeader from './tab-header/tab-header.component';
export default class Tabs extends Component {
  constructor(props) {
    super(props)
    this.selectTab = this.selectTab.bind(this);
    this.state = {
      activeTab: this.props.children[0].props.label
    }
  }
  //--------------------------------------------------------------------------
  selectTab(activeTab) {
    this.setState({activeTab})
  }
  //--------------------------------------------------------------------------
  render() {
    return (
      <div className="tab-container">
        <div className="tab">
          {this.props.children && 
          React.Children.map(this.props.children, tab =>
            <TabHeader 
              label={tab.props.label}
              activeTab={this.state.activeTab}
              onClick={this.selectTab}/>
          )}
        </div>
        {React.Children.map(
          this.props.children, 
          child => {
            return React.cloneElement(
              child, 
              {
                label: child.props.label,
                activeTab: this.state.activeTab
              }
            )
          }
        )}
      </div>
    );
  }
}