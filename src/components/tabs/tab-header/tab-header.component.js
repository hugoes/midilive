import React, { Component } from 'react';
import PropTypes from 'prop-types';
export default class TabHeader extends Component {
    //--------------------------------------------------------------------------
    static propTypes = {
        label: PropTypes.string,
        activeTab: PropTypes.string
    }
    //--------------------------------------------------------------------------
    render() {
        let className = 'tablinks';
        if(this.props.activeTab === this.props.label) className = `${className} active` 
        return (
            <button className={className} onClick={() => this.props.onClick(this.props.label)}>
                {this.props.label}
            </button>
        )    
    }
    //--------------------------------------------------------------------------
}