import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './tab-content.component.scss';

export default class Tab extends Component {
  static propTypes = {
    label: PropTypes.string,
    activeTab: PropTypes.string
  };
  //--------------------------------------------------------------------------
  render() {
    let className = 'tabcontent';
    if(this.props.activeTab === this.props.label) className = `${className} active`;
    return (
      <div className={className}>
        {this.props.children}
      </div>
    );
  }
}

