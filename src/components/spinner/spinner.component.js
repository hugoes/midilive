import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './spinner.component.scss';
import Iterator from '../../vendor/iterator.class'

export default class Spinner extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
    part: PropTypes.string.isRequired,
    onChangeValue: PropTypes.func.isRequired,
    value: PropTypes.object.isRequired,
    intitFromTree: PropTypes.bool
  }
  constructor(props) {
    super(props)
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.items = new Iterator(this.props.data);
    this.items.setIndex(this.props.value);
  }
  //--------------------------------------------------------------------------
  handleChange(event) {
    event.preventDefault();
  }
  //--------------------------------------------------------------------------
  handleClick(type) {
    let item = this.items[type]();
    // console.log(item);
    if(!item.done) {
      this.props.onChangeValue(item.value, this.props.part);
    }
  }
  //--------------------------------------------------------------------------
  render() {
    let className = 'spinner';
    if(this.props.intitFromTree) className = `${className} init-from-tree`;
    return (
      <div className={className}>
          <button onClick={() => this.handleClick('prev')}>-</button>
          <input type="text" value={this.props.value.value} onChange={this.handleChange}></input>
          <button onClick={() => this.handleClick('next')}>+</button>
      </div>
    );
  }
}