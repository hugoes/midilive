import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './instrument.component.scss';
import Spinner from '../spinner/spinner.component';
import IconButton from '../icon-button/icon-button.component';
import save from '../../assets/save.svg';
import mute from '../../assets/report_off.svg';
import add from '../../assets/ic_add_circle.svg';
import remove from '../../assets/ic_remove_circle.svg';
import note from '../../assets/ic_audiotrack.svg';
import InstrumentConfig from '../../assets/json/instrument';
import defaultSection from '../../assets/json/songs/default-section';
import defaultChord from '../../assets/json/songs/default-chord';
export default class Instrument extends Component {
    POINT = '.';
    //--------------------------------------------------------------------------------------------
    static propTypes = {
        type: PropTypes.string,
        content : PropTypes.object,
        contentIndex: PropTypes.number.isRequired,
        sectionIndex: PropTypes.number,
        onChangeValue: PropTypes.func.isRequired,
    };
    //--------------------------------------------------------------------------------------------
    static defaultProps = {
        sectionIndex : null,
        content : null
    };
    //--------------------------------------------------------------------------------------------
    constructor(props) {
        super(props);
        this.onChangeValue = this.onChangeValue.bind(this);
        this.addContent = this.addContent.bind(this);
        this.removeContent = this.removeContent.bind(this);
    }
    //--------------------------------------------------------------------------------------------
    onChangeValue(value, part) {
        const content = {...this.props.content};
        const [first, last] = part.split(this.POINT);
        if (last) content[first][last] = value;
        else content[first] = value;
        this.changeValue(content, this.props);
    }
    //--------------------------------------------------------------------------------------------
    addContent() {
        let content = this.props.sectionIndex ? defaultChord : defaultSection;
        this.changeValue(JSON.parse(JSON.stringify(content)), this.props);
    }
    //--------------------------------------------------------------------------------------------
    removeContent() {
        this.changeValue(null, this.props);
    }
    //--------------------------------------------------------------------------------------------
    changeValue(content, props) {
        props.onChangeValue(
            content,
            this.props.contentIndex,
            this.props.sectionIndex
        );
    } 
    //--------------------------------------------------------------------------------------------
    renderInstrument(keyZoneClass, isChord) {
        return (
            <React.Fragment>
            {/*Key Zone*/}
            {this.props.content.key_zone && 
            <div className={keyZoneClass}>
                <Spinner 
                    data={InstrumentConfig.key_zone.first} 
                    part={'key_zone.first'}
                    value={this.props.content.key_zone.first}
                    onChangeValue={this.onChangeValue}/>{/*Key Zone First*/}
                <Spinner
                    data={InstrumentConfig.key_zone.last}
                    part={'key_zone.last'}  
                    value={this.props.content.key_zone.last}
                    onChangeValue={this.onChangeValue}/>{/*Key Zone Last*/}
            </div>}
            {/*Midi Channel*/}
            <Spinner
                intitFromTree={isChord}
                data={InstrumentConfig.midi_channel}
                part={'midi_channel'}  
                value={this.props.content.midi_channel}
                onChangeValue={this.onChangeValue}/>
            {/*Level*/}
            <Spinner
                data={InstrumentConfig.level}
                part={'level'}   
                value={this.props.content.level}
                onChangeValue={this.onChangeValue}/>
            {/*Pan*/}
            <Spinner
                data={InstrumentConfig.pan}
                part={'pan'}   
                value={this.props.content.pan}
                onChangeValue={this.onChangeValue}/>
            {/*Tranpose*/}
            <Spinner
                data={InstrumentConfig.transpose} 
                part={'transpose'}  
                value={this.props.content.transpose}
                onChangeValue={this.onChangeValue}/>
            {/*Mute button*/}
            <IconButton svg={mute} alt="mute"/>
            {/*Save button*/}
            <IconButton
                hidden={isChord} 
                withState={false}
                svg={save} alt="save"/>
            {/*Notes button*/}
            <IconButton
                hidden={!isChord} 
                svg={note} alt="edit notes"/>
            {/*Remove button*/}
            <IconButton
                onClick={this.removeContent} 
                svg={remove} alt="delete"/>
            </React.Fragment>
        )
    }
    //--------------------------------------------------------------------------------------------
    render() {
        let keyZoneClass = 'key-zone';
        let isChord = false; //If is type chord

        if(this.props.type === 'chord') {
            keyZoneClass = `${keyZoneClass} hidden`;
            isChord = true;
        }
        return (
            <div className="instrument">
                {/*Section*/}
                <h2>{this.props.name}</h2>
                {this.props.content ?  (
                    this.renderInstrument(keyZoneClass, isChord)
                ) : (
                    <IconButton
                        onClick={this.addContent} 
                        svg={add} alt="add"/>
                )}
            </div>
        );
    }
}