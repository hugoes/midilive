import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './midi.component.scss';

export default class Midi extends Component {
  static propTypes = {
    midiApi: PropTypes.object.isRequired,
    midiInputs: PropTypes.array.isRequired,
    midiOutputs: PropTypes.array.isRequired,
    activeMidiInput: PropTypes.object.isRequired,
    selectedMidiOutputs: PropTypes.array.isRequired,
    addActiveOutput: PropTypes.func.isRequired,
    removeActiveOutput: PropTypes.func.isRequired
  }
  //--------------------------------------------------------------------------
  constructor(props) {
    super(props)
    this.handleMidiInputChange = this.handleMidiInputChange.bind(this);
    this.handleMidiOutputChange = this.handleMidiOutputChange.bind(this);
  }
  //--------------------------------------------------------------------------
  componentDidMount() {
      this.props.midiApi.getMidiAccess((midiAccess)=>{
        this.props.setInputs(Array.from(midiAccess.inputs.values()));
        this.props.setOutputs(Array.from(midiAccess.outputs.values()));
      });
  }
  //--------------------------------------------------------------------------
  componentWillReceiveProps(nextProps) {
    if (nextProps.activeMidiInput !== this.props.activeMidiInput) {
        this.props.midiApi.openMidiInput(nextProps.activeMidiInput);
    }
    if (nextProps.selectedMidiOutputs !== this.props.selectedMidiOutputs) {
        this.props.midiApi.setActiveOutputs(nextProps.selectedMidiOutputs)
    }
  }
  //--------------------------------------------------------------------------
  componentDidUpdate(prevProps) {
    if (prevProps.activeMidiInput !== this.props.activeMidiInput) {
        this.props.midiApi.closeMidiInput(prevProps.activeMidiInput);
    }
  }
  //--------------------------------------------------------------------------
  handleMidiInputChange(event) {
    this.props.setActiveInput(this.props.midiInputs[event.target.value]);
  }
  //--------------------------------------------------------------------------
  handleMidiOutputChange(event) {
    if(!event.target.checked) {
        this.props.removeActiveOutput(this.props.midiOutputs[event.target.value]);
    } else {
        if(this.props.selectedMidiOutputs.length > 2) {
            event.preventDefault(); 
            return;
        }
        this.props.addActiveOutput(this.props.midiOutputs[event.target.value]);
    }
  }
  //--------------------------------------------------------------------------
  render() {
    return (
        <div className="midi-container">
            {/*MIDI Inputs*/}
            <div className="col-6 midi-inputs-container">
                <div className="panel">
                    <div>Midi Inputs</div>
                    <div>
                        {this.props.midiInputs.length > 0 ? ( 
                            <select onChange={this.handleMidiInputChange}>
                                {this.props.midiInputs.map((input, index) => 
                                <option key={index} value={index}>
                                    {input.name}
                                </option>)}
                            </select>
                        ) : (
                            <h2>No hay midi inputs</h2>
                        )}
                    </div>
                </div>
            </div>
            {/*MIDI Outputs*/}
            <div className="col-6 midi-outputs-container">
                <div className="panel">
                    <div>Midi Outputs</div>
                    <div>
                        {this.props.midiOutputs.length > 0 ? (
                            <div className="midi-outputs-list">
                            {this.props.midiOutputs.map((output, index) =>
                                <div key={index} >
                                    <input type="checkbox"
                                        name={output.name} 
                                        value={index}
                                        onChange={this.handleMidiOutputChange}/> 
                                    <p>{output.name}</p>
                                </div>
                            )}
                            </div>
                        ) : (
                            <h2>No hay midi outputs</h2>
                        )}
                    </div>
                </div>
            </div>
      </div>
    );
  }
}
