import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './icon-button.component.scss';

export default class IconButton extends Component {
    static propTypes = {
        hidden : PropTypes.bool,
        onClick : PropTypes.func,
        withState: PropTypes.bool,
    };
    //--------------------------------------------------------------------------
    static defaultProps = {
        withState : true
    }
    //--------------------------------------------------------------------------
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.state = {
            pressed: false,
        }
    }
    //--------------------------------------------------------------------------
    handleClick(event) {
        event.preventDefault();
        if(this.props.withState) {
            this.setState(state => ({
                pressed: !state.pressed
            }));
        }
        if(this.props.onClick) this.props.onClick(event);
    }
    //--------------------------------------------------------------------------
    render() {
        let className = 'icon-button'; 
        className = this.state.pressed ? `${className} pressed` : className;
        className = this.props.hidden ? `${className} hidden` : className;
        return (
            <button className={className}
                onClick={this.handleClick}>
                <img src={this.props.svg} className="icon" alt={this.props.alt}/>
            </button>
        );
    }
    //--------------------------------------------------------------------------
}