import React, { Component } from 'react';
import './primary-button.component.scss';

export default class IconButton extends Component {
    //--------------------------------------------------------------------------
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    //--------------------------------------------------------------------------
    handleClick(event) {
        event.preventDefault();
    }
    //--------------------------------------------------------------------------
    render() {
        return (
            <div className='primary-btn'
                onClick={this.handleClick}>
                <img src={this.props.icon} 
                    className="icon" 
                    alt={this.props.alt}/>
                <span>{this.props.title}</span>
            </div>
        );
    }
    //--------------------------------------------------------------------------
}