
class Iterator {
    constructor(array) {
        this.index = 0;
        this.array = array;
        this.length = this.array.length;
        this.action = null;
    }
    next() {
        if(this.index < this.length-1) {
            return { value: this.array[++this.index], done: false };
        } else {
            return { done: true }; 
        } 
    }
    prev() {
        if(this.index > 0) {
            return { value: this.array[--this.index], done: false };
        } else {
            return { done: true }; 
        } 
    }
    setIndex(item) {
        // console.log('SET INDEX ITERATOR', item);
        const index = this.array.findIndex(i => i.index === item.index);
        if(index !== 1) {
            this.index = index;
        }
    }
}
export default Iterator;