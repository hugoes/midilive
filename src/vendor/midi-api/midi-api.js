export default class MidiApi  {
    NOT_SUPORT_ERROR = "Your browser doesn't support WebMIDI API.";
    constructor() {
        this.status = false;
        this.handleMidiMessage = this.handleMidiMessage.bind(this);
        this.setCurrentSong = this.setCurrentSong.bind(this);
        if (navigator.requestMIDIAccess) {
            this.status = true;
        } 
    }
    //-----------------------------------------------------------------------
    getMidiAccess(successCallback) {
        return navigator.requestMIDIAccess({sysex: false })
            .then(successCallback, this.onMIDIFailure);
    }
    //-----------------------------------------------------------------------
    openMidiInput(input) {
        if(input instanceof MIDIInput && input.connection === 'closed') {
            input.open().then(()=> {
                console.log(`[MIDI INPUT OPENED] => ${input.name}`);
                input.onmidimessage = this.handleMidiMessage;
            });
        }
    }
    //-----------------------------------------------------------------------
    closeMidiInput(input) {
        if(input instanceof MIDIInput && input.connection === 'open') {
            input.close().then(()=> {
                console.log(`[MIDI INPUT CLOSED] => ${input.name}`);
            });
        }
    }
    //-----------------------------------------------------------------------
    setCurrentSong(song) {
        if(!song) return;
        console.log('[CURRENT SONG]', song);
        this.song = song;
    }
    //-----------------------------------------------------------------------
    setActiveOutputs(outputs) {
        console.log('[SET ACTIVE OUTPUTS]', outputs);
        this.outputs = outputs;
    }
    //-----------------------------------------------------------------------
    changeMidiChannel(midiType, newChannel) {
        return '0x' + midiType.toString(16).replace(/0|1/, newChannel);
    }
    //-----------------------------------------------------------------------
    handleMidiMessage(message) {
        let [MIDI_TYPE, MIDI_BYTE_1, MIDI_BYTE_2] = message.data;
        if(this.song && this.outputs) {
            // ON NOTE ON OR ON NOTE OFF 
            if(MIDI_TYPE === 145 || MIDI_TYPE ===  129) {
                for (let section of this.song.sections) {
                    if (MIDI_BYTE_1 >= section.key_zone.first.index && MIDI_BYTE_1 <= section.key_zone.last.index) {
                        //CHANGE MIDI CHANNEL
                        const changed = this.changeMidiChannel(MIDI_TYPE, section.midi_channel.index);
                        //SEND MIDI MESSAGE
                        this.outputs[section.output.index].send([
                            changed, 
                            MIDI_BYTE_1 + section.transpose.value , 
                            MIDI_BYTE_2
                        ]);
                        break;
                    }
                }
            }
            // ON CONTROL 
            else { 
               if(MIDI_BYTE_1 !== 1 && MIDI_BYTE_1 !== 10) {
                    this.song.sections.forEach(section => {
                        //CHANGE MIDI CHANNEL
                        const changed = this.changeMidiChannel(MIDI_TYPE, section.midi_channel.index);
                        //IF LEVEL
                        if(MIDI_BYTE_1 === 7) MIDI_BYTE_1 = MIDI_BYTE_1 + section.level.value
                        this.outputs[section.output.index].send([changed, MIDI_BYTE_1, MIDI_BYTE_2]);
                    });
                }
                
            }
        } 
    }
    //-----------------------------------------------------------------------
    onMIDIFailure(e) {
      // when we get a failed response, run this code
      console.log(this.NOT_SUPORT_ERROR);
    }
    //----------------------------------------------------------------------- 
}
